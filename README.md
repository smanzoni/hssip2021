# HSSIP2021

## Measuring Higgs boson mass

The Higgs boson mass (mH) is one of the free parameter of the Standard Model, which is the theory model that better describes the interaction of subatomic particles.

Determine the Higgs boson mass is important in order to predict the production and the decay rate of the Higgs boson itself as shown from the two following plots.

![production rate vs mH](https://twiki.cern.ch/twiki/pub/LHCPhysics/LHCHWGCrossSectionsFigures/plotAll_13tev_BSM_sqrt.pdf) ![decay rate vs mH](https://twiki.cern.ch/twiki/pub/LHCPhysics/LHCHWGCrossSectionsFigures/Higgs_BR_LM.pdf)

We are going to measure the Higgs boson mass using the decay of the Higgs boson in two photons. This decay is rarer then other decay process. The Higgs boson decays in two photons only the 0.227% of its decays. Thanks to the exceptional energy resolution that we are able to reach in recostructing the photons inside the ATLAS experiment we can select H->yy events as a well peaked resonance over a smooth decreasing background. The expected resolution of the Higgs boson peak reconstructed from two photons is between 1.5 and 2 GeV.
The mass value of the Higgs boson is extracted from a signal+background fit on the data where both the resonant signal and the background are modelled with functional form. The mass of the Higgs boson is then extracted from the peak of the signal functional form.

![fit of m_yy](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2016-33/fig_02b.pdf)

### Additional references (to be read):

Last Higgs boson mass measurement at ATLAS https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2016-33/

![Mass results](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2016-33/fig_04.pdf)


## Exercise 1

Plot the distribution of the invariant mass of the two photons from Monte Carlo simulation generated at different mH. The Monte Carlo simulation contains events that are not true data, but they are generated starting from theoretical calculation. These events are then pass through the simulation of the ATLAS detector and processed by the detector reconstruction to emulate the real data collected at the LHC.

When enterign lxplus use the following setup:

`source /cvmfs/sft.cern.ch/lcg/views/LCG_101/x86_64-centos7-gcc8-opt/setup.sh`

For mH=125 GeV the MC events are stored in: `/afs/cern.ch/work/s/smanzoni/public/HISSIP_2021/Signal_yy/125GeV.root`

The name of the TTree is: `myCatNtuple`

The name of the m_yy branch is: `m_yy`

example of a code

```
#include <TCanvas.h>

void HistoPlot (){
    gROOT->SetStyle("ATLAS");
    ROOT::EnableImplicitMT();
    // Uplaod data frame to read the TTree in the file
    ROOT::RDataFrame df125("myCatNtuple", "/afs/cern.ch/work/s/smanzoni/public/HISSIP_2021/Signal_yy/125GeV.root");
    // Define the histogram
    auto h125 = df125.Histo1D({"hist125","hist125",100,120E3,130E3},"m_yy","EventWeight");
    // Scale for the correct luminosity
    h125->Scale(139000.);
    TCanvas* c= new TCanvas("c","c",800,800);
    h125->GetXaxis()->SetTitle ("m_{#gamma#gamma} [MeV]");
    h125->GetYaxis()->SetTitle ("Nevents");
    h125->Draw("Hist");
    c->SaveAs("m_yy_125.pdf");
}
```



### Exercise 1.1

Repeat the same procedure for each  available MC sample generated with different mH

### Exercise 1.2

Produce a single plot with all the m_yy distributions coming from the different MC samples 

### Exercise 1.3

Produce the plot of the m_yy distribution of the data collected in 2016. The data are stored in:

`/afs/cern.ch/work/s/smanzoni/public/HISSIP_2021/Data_yy/data2016_blind.root`

To be notice no `"EventWeight"` or scaling (e.g. `->Scale(139000)` ) are needed for the real data

## Exercise 2

In order to reduce the contamination of background in the data several selection are applied to the events. Check the effects on the MC. Plot the m_yy distribtuion before and after the selection on the same plot. For the two distribution plot the Mean, StdDev and Nevents

```
#include <TCanvas.h>
#include <TLegend.h>
#include <TLatex.h>
#include <string>

void HistoPlot_sel (){
    gROOT->SetStyle("ATLAS");
    ROOT::EnableImplicitMT();
    // Uplaod data frame to read the TTree in the file
    ROOT::RDataFrame df125("myCatNtuple", "/afs/cern.ch/work/s/smanzoni/public/HISSIP_2021/Signal_yy/125GeV.root");
    // Define the histogram
    auto h125 = df125.Histo1D({"hist125","hist125",100,115E3,135E3},"m_yy","EventWeight");
    auto h125_sel = df125.Filter("isPassed").Histo1D({"hist125_sel","hist125_sel",100,115E3,135E3},"m_yy","EventWeight");
    // Scale for the correct luminosity
    h125->Scale(139000.);
    h125_sel->Scale(139000.);
    h125->SetLineColor(kRed);
    h125_sel->SetLineColor(kBlue);
    TCanvas* c= new TCanvas("c","c",800,800);
    h125->GetXaxis()->SetTitle ("m_{#gamma#gamma} [MeV]");
    h125->GetYaxis()->SetTitle ("Nevents");
    auto h125_root = h125->DrawCopy("HIST");
    auto h125_sel_root = h125_sel->DrawCopy("HIST SAME");
    TLegend* legend = new TLegend(0.2, 0.65, 0.5, 0.90);
    legend->SetBorderSize(0);
    legend->SetFillStyle(0);
    legend->AddEntry(h125_root, "MC m_{H}=125 GeV", "L" );
    std::string nevents125="Nevents: "+to_string(h125->Integral());
    legend->AddEntry("",nevents125.c_str(), "" );
    std::string mean125="Mean: "+to_string(h125->GetMean());
    legend->AddEntry("",mean125.c_str() , "" );
    std::string stddev125="StdDev: "+to_string(h125->GetStdDev());
    legend->AddEntry("",stddev125.c_str(), "" );
    legend->AddEntry(h125_sel_root, "MC m_{H}=125 GeV after sel.", "L" );
    std::string nevents125_sel="Nevents: "+to_string(h125_sel->Integral());
    legend->AddEntry("",nevents125_sel.c_str(), "" );
    std::string mean125_sel="Mean: "+to_string(h125_sel->GetMean());
    legend->AddEntry("",mean125_sel.c_str() , "" );
    std::string stddev125_sel="StdDev: "+to_string(h125_sel->GetStdDev());
    legend->AddEntry("",stddev125_sel.c_str(), "" );
    legend->Draw();
    c->SaveAs("m_yy_125_sel.pdf");
}
```




### Exercise 2.1

Repeat the same procedure for each  available MC sample generated with different mH

### Exercise 2.2

Produce a single plot with all the m_yy distributions coming from the different MC samples before and after the selection

### Exercise 2.3

Produce the plot of the m_yy distribution of the data collected in 2016 before and after the selection

## Exercise 3

One of the fundamental ingredient to measure the higgs boson mass is the functional form that describes the signal event distribution in m_yy. We use a very special function know as Double Sided Crystall Ball (DSCB). See https://root.cern/doc/v624/classRooCrystalBall.html
In this exercise you will need to find the parameter that describes this function fitting the DSCB on the MC sample at 125 GeV usign the RooFit package of ROOT. The parameters are printed with the output of the fit.

Example of code:

```
#include <fstream>
void Fit_sel (){
    gROOT->SetStyle("ATLAS");
    ROOT::EnableImplicitMT();
    // Uplaod data frame to read the TTree in the file
    ROOT::RDataFrame df125("myCatNtuple", "/afs/cern.ch/work/s/smanzoni/public/HISSIP_2021/Signal_yy/125GeV.root");
    // Define the histogram
    auto h125_sel = df125.Filter("isPassed").Histo1D({"hist125_sel","hist125_sel",200,115E3,135E3},"m_yy","EventWeight");
    h125_sel->Scale(139000.);
    // Define variable to be used by the RooDataSet
    RooRealVar m_yy("m_yy", "m_yy", 115000,   135000.);
    // Define input dataset from histogram
    RooDataHist* dh = new RooDataHist("dh", "dh", RooArgList(m_yy), RooFit::Import(*h125_sel));
    // Define the parameter of the DSCB
    RooRealVar mu("mu", "mu", 125000., 115000.,   135000.);
    RooRealVar sigma("sigma", "sigma", 1500., 100.,  5000.);
    RooRealVar alphaL("alphaL", "alphaL",1., 0.01,   5.);
    RooRealVar alphaR("alphaR", "alphaR",1., 0.01,   5.);
    RooRealVar nL("nL", "nL", 1.,0.01,   1.E4);
    RooRealVar nR("nR", "nR", 1.,0.01,   1.E4);
    // Define the DSCB
    RooCrystalBall* DSCB = new RooCrystalBall	("DSCB", "DSCB", m_yy, mu, sigma, alphaL,	nL, alphaR,	nR );
    // Fit the DSCB to the histogram
    RooFitResult *fit_res =DSCB->fitTo(*dh, RooFit::SumW2Error(true), RooFit::Offset(true), RooFit::Save());	
    //Draw dataset and fitted DSCB 
    TCanvas* c= new TCanvas("c","c",800,800);
    // Divide the canvas in two and other graphics stuff...
    double r = 0.35;
    double epsilon = 0.02;
    TPad *pad_bottom =new  TPad("pad_bottom", "pad_bottom", 0, 0., 1, r * (1 - epsilon));
    pad_bottom->SetFrameFillStyle(4000);
    TPad* pad_top = new TPad("pad_top", "pad_top", 0, r - epsilon, 1, 1);
    pad_top->SetFrameFillStyle(4000);
    pad_top->SetMargin(0.15, 0.03, epsilon, 0.04);
    pad_bottom->SetMargin(0.15, 0.03, 0.3, 0.0);
    pad_top->SetBottomMargin(epsilon);
    pad_bottom->SetTopMargin(0);
    pad_bottom->SetFillColor(0);
    pad_bottom->SetFrameFillStyle(0);
    pad_bottom->SetFillStyle(0);
    pad_top->SetFillColor(0);
    pad_top->SetFrameFillStyle(0);
    pad_top->SetFillStyle(0);
    //Set Logaritmich scale 
    pad_top->SetLogy();
    c->cd();
    pad_bottom->Draw();
    c->cd();
    pad_top->Draw();
    pad_top->cd();
    double offset_y = 1.0;
    // Drawing the frame with the dataset
    RooPlot * frame = m_yy.frame();
    frame->GetYaxis()->SetLabelSize(0.041);
    frame->GetYaxis()->SetTitleSize(0.043);
    frame->GetYaxis()->SetTitleOffset(1.21 * offset_y);
    frame->GetYaxis()->SetTitle("Events [MeV^{-1}]");
    frame->SetLabelSize(0.);
    frame->SetTitleSize(0.);
    dh->plotOn(frame,RooFit::DataError(RooAbsData::SumW2));
    // Drawing the DSCB
    DSCB->plotOn(frame,RooFit::LineColor(kBlue));
    frame->Draw();
    pad_bottom->cd();
    //Drawing  data-fit histogram
    RooHist* hresid= frame->residHist();
    RooPlot * frame2 = m_yy.frame();
    frame2->GetYaxis()->SetTitle("Data-Fit");
    frame2->GetXaxis()->SetTitle("m_{#gamma#gamma} [MeV]");
    frame2->addPlotable(hresid,"P");
    frame2->GetXaxis()->SetTitleSize(0.10);
    frame2->GetXaxis()->SetTitleOffset(1.25);
    frame2->GetXaxis()->SetLabelSize(0.081);
    frame2->GetYaxis()->SetLabelSize(0.077);
    frame2->GetYaxis()->SetTitleSize(0.077);
    frame2->GetYaxis()->SetTitleOffset(0.66 * offset_y);
    frame2->GetYaxis()->CenterTitle(true);
    frame2->Draw();
    c->SaveAs("m_yy_125_fit.pdf");
    ofstream myfile;
    myfile.open ("125_parameter.txt");
    myfile << "mu: "<<mu.getVal()<<".\n";
    myfile << "sigma: "<<sigma.getVal() <<".\n";
    myfile << "alphaL: "<<alphaL.getVal() <<".\n";
    myfile << "nL: "<<nL.getVal() <<".\n";
    myfile << "alphaR: "<<alphaR.getVal() <<".\n";
    myfile << "nR: "<<nR.getVal() <<".\n";
    myfile.close();
    return;
}
```





### Exercise 3.1

Find the paramenteres of the DSCB for the different hypotheses of mH

### Exercise 3.2 

Plot the values of each parameter vs mH using a TGraph


## Exercise 4

Now we can build up a functional form that is able to describe the signal shape for any mH value. 

### Exercise 4.1
As first step, starting from the code prepared for exercise 3.2, we would like to extract the linear dependence of the DSCB as a function of mH. The idea is to fit with a first order polynomial the graph obtained in Exercise 3.2. See following example:

```
void plot_mu_mH(){
    gROOT->SetStyle("ATLAS");
    double mu[8]={110021.,123023.,124021,125043,125997,127059,130155,140028};
    double mH[8]={110000,123000,124000,125000,126000,127000,130000,140000};
    // Define TGraph
    TGraph * g_mu = new TGraph(); 
    // Fill the TGraph
    for (int i=0; i<8; i++){
        g_mu->SetPoint(i,mH[i],mu[i]);
    }

    TF1 *f = new TF1("f", "[0] + [1] * x ");

    TCanvas* c= new TCanvas("c","c",800,800);
    g_mu->Fit(f);
    g_mu->GetXaxis()->SetTitle ("m_{H} [GeV]");
    g_mu->GetYaxis()->SetTitle ("#mu");
    g_mu->Draw("AP");
    c->SaveAs("mu_vs_mH.pdf");

    std::cout<<"Fitting mu with linear polynomial as function of MH: mu=a+b*MH"<<std::endl;
    std::cout<<"parameter a is: "<<f->GetParameter(0)<<std::endl;
    std::cout<<"parameter b is: "<<f->GetParameter(1)<<std::endl;

}

```


### Exercise 4.2
Do the same also for the other parameters of the DSCB.

### Exercise 4.3

Using a code similar to the one developed in the Exercise 3 buil a DSCB that is parametric in mH and compared it with the DSCB fitted at each value of mH


```
void Fit_sel_parametric (){
    gROOT->SetStyle("ATLAS");
    ROOT::EnableImplicitMT();
    // Uplaod data frame to read the TTree in the file
    ROOT::RDataFrame df125("myCatNtuple", "/afs/cern.ch/work/s/smanzoni/public/HISSIP_2021/Signal_yy/125GeV.root");
    // Define the histogram
    auto h125_sel = df125.Filter("isPassed").Histo1D({"hist125_sel","hist125_sel",550,105E3,160E3},"m_yy","EventWeight");
    h125_sel->Scale(139000.);
    // Define variable to be used by the RooDataSet
    RooRealVar m_yy("m_yy", "m_yy", 115000,   135000.);
    // Define input dataset from histogram
    RooDataHist* dh = new RooDataHist("dh", "dh", RooArgList(m_yy), RooFit::Import(*h125_sel));
    // Define the parameter of the DSCB
    RooRealVar mu("mu", "mu", 125000., 120000.,   130000.);
    RooRealVar sigma("sigma", "sigma", 1500., 100.,  5000.);
    RooRealVar alphaL("alphaL", "alphaL",1., 0.01,   5.);
    RooRealVar alphaR("alphaR", "alphaR",1., 0.01,   5.);
    RooRealVar nL("nL", "nL", 1.,0.01,   1.E4);
    RooRealVar nR("nR", "nR", 1.,0.01,   1.E4);
    // Define the DSCB
    RooCrystalBall* DSCB = new RooCrystalBall	("DSCB", "DSCB", m_yy, mu, sigma, alphaL,	nL, alphaR,	nR );

    //Define the parametrice DSCB
    RooRealVar mH("mH", "mH", 125000.); //is constant
    RooRealVar mu_a("mu_a", "mu_a", -142.016); //is constant
    RooRealVar mu_b("mu_b", "mu_b", 1.00148); //is constant
    RooFormulaVar mu_linear("mu_linear", "@0+@1*@2", RooArgList(mu_a,mu_b,mH));
    RooRealVar sigma_a("sigma_a", "sigma_a", 0); //is constant
    RooRealVar sigma_b("sigma_b", "sigma_b", 0); //is constant
    RooFormulaVar sigma_linear("sigma_linear", "@0+@1*@2", RooArgList(sigma_a,sigma_b,mH));
    RooRealVar alphaL_mean("alphaL_mean", "alphaL_mean",0.); //is constant!
    RooRealVar alphaR_mean("alphaR_mean", "alphaR_mean",0.); //is constant!
    RooRealVar nL_mean("nL_mean", "nL_mean", 0.); //is constant!
    RooRealVar nR_mean("nR_mean", "nR_mean", 0.); //is constant!

    // Add here the dependencies for the other parameter
    RooCrystalBall* DSCB_par = new RooCrystalBall("DSCB_para", "DSCB_para", m_yy, mu_linear, sigma_linear, alphaL_mean,nL_mean, alphaR_mean,nR_mean );

    // Fit the DSCB to the histogram
    RooFitResult *fit_res_1 =DSCB_par->fitTo(*dh, RooFit::SumW2Error(true), RooFit::Offset(true), RooFit::Save()); //only for normalization, not modifying the parameters
    RooFitResult *fit_res =DSCB->fitTo(*dh, RooFit::SumW2Error(true), RooFit::Offset(true), RooFit::Save()); 
    //Draw dataset and fitted DSCB 
    TCanvas* c= new TCanvas("c","c",800,800);
    // Divide the canvas in two and other graphics stuff...
    double r = 0.35;
    double epsilon = 0.02;
    TPad *pad_bottom =new  TPad("pad_bottom", "pad_bottom", 0, 0., 1, r * (1 - epsilon));
    pad_bottom->SetFrameFillStyle(4000);
    TPad* pad_top = new TPad("pad_top", "pad_top", 0, r - epsilon, 1, 1);
    pad_top->SetFrameFillStyle(4000);
    pad_top->SetMargin(0.15, 0.03, epsilon, 0.04);
    pad_bottom->SetMargin(0.15, 0.03, 0.3, 0.0);
    pad_top->SetBottomMargin(epsilon);
    pad_bottom->SetTopMargin(0);
    pad_bottom->SetFillColor(0);
    pad_bottom->SetFrameFillStyle(0);
    pad_bottom->SetFillStyle(0);
    pad_top->SetFillColor(0);
    pad_top->SetFrameFillStyle(0);
    pad_top->SetFillStyle(0);
    //Set Logaritmich scale 
    pad_top->SetLogy();
    c->cd();
    pad_bottom->Draw();
    c->cd();
    pad_top->Draw();
    pad_top->cd();
    double offset_y = 1.0;
    // Drawing the frame with the dataset
    RooPlot * frame = m_yy.frame();
    frame->GetYaxis()->SetLabelSize(0.041);
    frame->GetYaxis()->SetTitleSize(0.043);
    frame->GetYaxis()->SetTitleOffset(1.21 * offset_y);
    frame->GetYaxis()->SetTitle("Events [MeV^{-1}]");
    frame->SetLabelSize(0.);
    frame->SetTitleSize(0.);
    dh->plotOn(frame,RooFit::Name("dh"),RooFit::DataError(RooAbsData::SumW2));
    // Drawing the DSCB
    DSCB->plotOn(frame,RooFit::Name("DSCB"),RooFit::LineColor(kBlue));
    DSCB_par->plotOn(frame,RooFit::Name("DSCB_para"),RooFit::LineColor(kRed));
    frame->Draw();
    pad_bottom->cd();
    //Drawing  data-fit histogram
    RooHist* hresid= frame->residHist("dh","DSCB");
    hresid->SetMarkerColor(kBlue);
    hresid_para->SetMarkerStyle(26);
    hresid->SetLineColor(kBlue);
    RooHist* hresid_para= frame->residHist("dh","DSCB_para");
    hresid_para->SetMarkerColor(kRed);
    hresid_para->SetMarkerStyle(25);
    hresid_para->SetLineColor(kRed);
    RooPlot * frame2 = m_yy.frame();
    frame2->GetYaxis()->SetTitle("Data-Fit");
    frame2->GetXaxis()->SetTitle("m_{#gamma#gamma} [MeV]");
    frame2->addPlotable(hresid,"P");
    frame2->addPlotable(hresid_para,"P");
    frame2->GetXaxis()->SetTitleSize(0.10);
    frame2->GetXaxis()->SetTitleOffset(1.25);
    frame2->GetXaxis()->SetLabelSize(0.081);
    frame2->GetYaxis()->SetLabelSize(0.077);
    frame2->GetYaxis()->SetTitleSize(0.077);
    frame2->GetYaxis()->SetTitleOffset(0.66 * offset_y);
    frame2->GetYaxis()->CenterTitle(true);
    frame2->Draw();
    c->SaveAs("m_yy_125_fit_para.pdf");
    // m_yy.setBins(440);
    // auto rooDataSet = dd.Book<double, double>(
    //     RooDataSetHelper("dataset125", // Name
    //     "Title of dataset",     // Title
    //     RooArgSet(m_yy, EventWeight)         // Variables in this dataset
    //     ),
    //     {"m_yy", "EventWeight"}                  // Column names in RDataFrame.
    // );
    return;
}
```

## Exercise 5

Using the parametrization defined in the previous steps evaluate the value of the unknown mass of the Higgs boson over four datasets.




```
void Fit_sig_bkg (){

    gROOT->SetStyle("ATLAS");
    ROOT::EnableImplicitMT();
    // Upload the dataset. 
    TFile * filein = new TFile("/afs/cern.ch/work/s/smanzoni/public/HISSIP_2021/mass_fit_ws.root");
    RooWorkspace * w= (RooWorkspace*) filein->Get("workspace");
    //Uncomment one of the following lines
    // RooDataSet* dataset=(RooDataSet *) w->obj("asimovData1"); //dataset1
    RooDataSet* dataset=(RooDataSet *) w->obj("asimovData2"); //dataset2
    // RooDataSet* dataset=(RooDataSet *) w->obj("asimovData3"); //dataset3
    // RooDataSet* dataset=(RooDataSet *) w->obj("asimovData4"); //dataset4
    RooRealVar m_yy("m_yy", "m_yy", 105000.,   160000.);
    RooRealVar mH("mH", "mH", 125000.,120000.,130000.); //is not constant!
    //Define the parametrice DSCB !! Use your values !
    RooRealVar mu_a("mu_a", "mu_a", 0); //is constant,
    RooRealVar mu_b("mu_b", "mu_b", 0.); //is constant 
    RooFormulaVar mu_linear("mu_linear", "@0+@1*@2", RooArgList(mu_a,mu_b,mH));
    RooRealVar sigma_a("sigma_a", "sigma_a", 0.); //is constant,  we are using GeV divide by 1000
    RooRealVar sigma_b("sigma_b", "sigma_b", 0.); //is constant
    RooFormulaVar sigma_linear("sigma_linear", "@0+@1*@2", RooArgList(sigma_a,sigma_b,mH));
    RooRealVar alphaL_mean("alphaL_mean", "alphaL_mean",0.); //is constant!
    RooRealVar alphaR_mean("alphaR_mean", "alphaR_mean",0.); //is constant!
    RooRealVar nL_mean("nL_mean", "nL_mean", 0.); //is constant!
    RooRealVar nR_mean("nR_mean", "nR_mean", 0.); //is constant!

    RooCrystalBall* DSCB_par = new RooCrystalBall("DSCB_para", "DSCB_para", m_yy, mu_linear, sigma_linear, alphaL_mean,nL_mean, alphaR_mean,nR_mean );


    //Define the bkg function
    
    RooRealVar a("a", "a", -4.1, -100.,   100.);
    // RooExponential* bkg= new RooExponential("exp", "exp", m_yy, a);
	RooGenericPdf bkg("exp", "exp","TMath::Exp(@1*(@0-100000.)/100000.)", RooArgList(m_yy, a));
    //Define the combined fuction sig+bkg
    RooRealVar n_bkg ("n_bkg", "Number of background events", 179671, 0., 1.E6);
    RooRealVar n_sig ("n_sig", "Number of signal events", 1224.2, 0., 1.E5);
    RooAddPdf* model = new RooAddPdf("model", "model", RooArgList(*DSCB_par,bkg), RooArgList(n_sig, n_bkg));

    // Fit the DSCB to the histogram
    RooFitResult *fit_res =model->fitTo(*dataset, RooFit::Save(), RooFit::Minos(true)); 
    //Draw dataset and fitted DSCB 
    TCanvas* c= new TCanvas("c","c",800,800);
    // Divide the canvas in two and other graphics stuff...
    double r = 0.35;
    double epsilon = 0.02;
    TPad *pad_bottom =new  TPad("pad_bottom", "pad_bottom", 0, 0., 1, r * (1 - epsilon));
    pad_bottom->SetFrameFillStyle(4000);
    TPad* pad_top = new TPad("pad_top", "pad_top", 0, r - epsilon, 1, 1);
    pad_top->SetFrameFillStyle(4000);
    pad_top->SetMargin(0.15, 0.03, epsilon, 0.04);
    pad_bottom->SetMargin(0.15, 0.03, 0.3, 0.0);
    pad_top->SetBottomMargin(epsilon);
    pad_bottom->SetTopMargin(0);
    pad_bottom->SetFillColor(0);
    pad_bottom->SetFrameFillStyle(0);
    pad_bottom->SetFillStyle(0);
    pad_top->SetFillColor(0);
    pad_top->SetFrameFillStyle(0);
    pad_top->SetFillStyle(0);
    //Set Logaritmich scale 
    // pad_top->SetLogy();
    c->cd();
    pad_bottom->Draw();
    c->cd();
    pad_top->Draw();
    pad_top->cd();
    double offset_y = 1.0;
    // Drawing the frame with the dataset
    RooPlot * frame = m_yy.frame();
    frame->GetYaxis()->SetLabelSize(0.041);
    frame->GetYaxis()->SetTitleSize(0.043);
    frame->GetYaxis()->SetTitleOffset(1.21 * offset_y);
    frame->GetYaxis()->SetTitle("Events [MeV^{-1}]");
    frame->SetLabelSize(0.);
    frame->SetTitleSize(0.);
    dataset->plotOn(frame,RooFit::Name("data"),RooFit::DataError(RooAbsData::Poisson));
    // Drawing the DSCB
    model->plotOn(frame,RooFit::Name("model"),RooFit::LineColor(kRed));
    model->plotOn(frame,RooFit::Name("bkg"),RooFit::Components(bkg),RooFit::LineStyle(7),RooFit::LineColor(kBlue)),
    frame->Draw();
    TLegend* leg = new TLegend(0.6, 0.65, 0.9, 0.90);
    leg->SetBorderSize(0);
    leg->SetFillStyle(0);
    leg->AddEntry(frame->findObject("data"),"Mock Data","PL");
    leg->AddEntry(frame->findObject("model"),"Signal+Bkg","L");
    leg->AddEntry(frame->findObject("bkg"),"Bkg","L");
    std::string mass_str="m_{H}= "+to_string(mH.getVal())+" MeV";
    leg->AddEntry("",mass_str.c_str(), "" );
    leg->Draw();
    pad_bottom->cd();
    //Drawing  data-fit histogram
    RooHist* hresid= frame->residHist("data","bkg");
    hresid->SetMarkerColor(kBlack);
    hresid->SetLineColor(kBlack);
    RooPlot * frame2 = m_yy.frame();
    frame2->GetYaxis()->SetTitle("Data-Bkg");
    frame2->GetXaxis()->SetTitle("m_{#gamma#gamma} [MeV]");
    frame2->addPlotable(hresid,"P");
    frame2->GetXaxis()->SetTitleSize(0.10);
    frame2->GetXaxis()->SetTitleOffset(1.25);
    frame2->GetXaxis()->SetLabelSize(0.081);
    frame2->GetYaxis()->SetLabelSize(0.077);
    frame2->GetYaxis()->SetTitleSize(0.077);
    frame2->GetYaxis()->SetTitleOffset(0.66 * offset_y);
    frame2->GetYaxis()->CenterTitle(true);
    frame2->Draw();
    c->SaveAs("mass_sig_bkg.pdf");

    std::cout<<"dataset 2 MH values is: "<<mH.getVal()<<" MeV  +/- "<<mH.getErrorHi()<<"/"<< mH.getErrorLo()<<" MeV"<<std::endl;

    return;
}
```




